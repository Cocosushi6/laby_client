package com.laby.world;

import java.util.ArrayList;

public class Map {
	
	private ArrayList<Tile> tiles = new ArrayList<Tile>();
	private ArrayList<Tile> solids = new ArrayList<Tile>();
	private int worldWidth = 0, worldHeight = 0;
	
	public Tile getTileAt(int x, int y) {
		for(Tile t : this.tiles) {
			if(t.getX() <= x && t.getX() + t.getWidth() >= x) {
				if(t.getY() <= y && t.getY() + t.getHeight() >= y) {
					return t;
				}
			}
		}
		return null;
	}
	
	public int getWorldWidth() {
		return this.worldWidth;
	}
	
	public int getWorldHeight() {
		return this.worldHeight;
	}
	
	public void setWorldWidth(int worldWidth) {
		this.worldWidth = worldWidth;
	}
	
	public void setWorldHeight(int worldHeight) {
		this.worldHeight = worldHeight;
	}
	
	
	public ArrayList<Tile> getTiles() {
		return this.tiles;
	}
	
	public ArrayList<Tile> getSolids() {
		return this.solids;
	}
}
