package com.laby.world;

import static org.lwjgl.opengl.GL11.*;

public class Wall extends Tile {
	
	protected String orientation = "horizontal";
	protected String subtype = "normal";
	protected float rotation = 0f;
	
	public Wall(int xPos, int yPos, int width, int height, String subtype, String orientation, boolean solid) {
		super(xPos, yPos, width, height, "wall", solid);
		this.orientation = orientation;
		this.subtype = subtype;
		switch(orientation) {
			case "left" :
				rotation = 270f;
				break;
			case "right" :
				rotation = 90f;
				break;
			case "down" : 
				rotation = 180f;
				break;
			case "up" : 
				rotation = 0f;
				break;
			case "horizontal" : 
				rotation = 0f;
				break;
			case "vertical" : 
				rotation = 180f;
				break;
			case "none" : 
				rotation = 0f;
				break;
		}
	}
	
	@Override
	public void render() {
		textures.get(this.type).bind();
		glPushMatrix();
			glTranslatef(this.x, this.y, 0);
				glBegin(GL_QUADS);
					glTexCoord2f(0, 0);
					glVertex2f(0, 0);
					
					glTexCoord2f(0, 1);
					glVertex2f(0, this.height);
					
					glTexCoord2f(1, 1);
					glVertex2f(this.width, this.height);
					
					glTexCoord2f(1, 0);
					glVertex2f(this.width, 0);
				glEnd();
		glPopMatrix();
	}
	
}
