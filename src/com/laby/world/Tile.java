package com.laby.world;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Tile {
	protected int x, y;
	protected String type;
	protected int width = 0, height = 0;
	protected HashMap<String, Texture> textures = new HashMap<String, Texture>();
	protected boolean solid = false;

	public Tile(int xPos, int yPos, int width, int height, String type, boolean solid) {
		this.x = xPos;
		this.y = yPos;
		this.type = type;
		this.width = width;
		this.height = height;
		this.solid = solid;
		try {
			textures.put(this.type, TextureLoader.getTexture("PNG", new FileInputStream(new File("res/" + this.type + ".png"))));
		} catch(IOException e) {
			e.printStackTrace();
		}
 	}
	
	public void render() {
		textures.get(this.type).bind();
		glPushMatrix();
		glTranslatef(this.x, this.y, 0);
		glBegin(GL_QUADS);
			glTexCoord2f(0, 0);
			glVertex2f(0, 0);
			glTexCoord2f(1, 0);
			glVertex2f(width, 0);
			glTexCoord2f(1, 1);
			glVertex2f(width, height);
			glTexCoord2f(0, 1);
			glVertex2f(0, height);
		glEnd();
		glPopMatrix();
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)this.x, (int)this.y, width, height);
	}
	
	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public String getType() {
	    return this.type;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public boolean isSolid() {
		return this.solid;
	}
	
	public void setSolid(boolean solid) {
		this.solid = solid;
	}
}
