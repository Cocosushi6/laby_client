package com.laby.gameplay;

public class Character {
	private int health;
	private int mana;
	private int defense;
	private int attack;
	
	public Character(int health, int mana) {
		this.health = health;
		this.mana = mana;
	}
	
	public void giveDamage(int damage) {
		if(health - damage >= 0) {
			health -= damage;
		} else {
			health = 0;
		}
	}
	
	public int getHealth() {
		return this.health;
	}
	
	public int getMana() {
		return this.mana;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}
	
}
