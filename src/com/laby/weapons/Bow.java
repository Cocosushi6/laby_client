package com.laby.weapons;

import java.util.ArrayList;

import com.laby.entities.Entity;
import com.laby.entities.EntityArrow;
import com.laby.main.Game;

public class Bow extends Weapon {
	
	private ArrayList<EntityArrow> arrows = new ArrayList<EntityArrow>();
	private int lastArrow = 19;
	
	
	public Bow(Game context, Entity belongsTo, String name) {
		super(context, belongsTo, name);
		for(int i = 0; i < 20; i++) {
			EntityArrow arrow = new EntityArrow(context, false);
			arrows.add(arrow);
		}
	}
	
	@Override
	public void use(String direction) {
		EntityArrow arrow = arrows.get(lastArrow);
		arrow = new EntityArrow(belongingTo.getx() + (int)belongingTo.getBounds().getWidth() / 2, belongingTo.gety() + (int)belongingTo.getBounds().getHeight() / 2, 40, direction, context, false);
		this.context.createRemoteEntity(arrow, "arrow");
		if(lastArrow > 0) {
			lastArrow--;
		}
	}
}