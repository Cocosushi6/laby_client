package com.laby.weapons;

import com.laby.entities.Entity;
import com.laby.main.Game;

public class Weapon {
	protected Entity belongingTo;
	protected Game context;
	protected String name;
	
	public Weapon(Game context, Entity belongsTo, String name) {
		this.context = context;
		this.belongingTo = belongsTo;
		this.name = name;
	}
	
	public void use(String direction) {
		
	}

	public Entity getBelongingTo() {
		return belongingTo;
	}

	public void setBelongingTo(Entity belongingTo) {
		this.belongingTo = belongingTo;
	}

	public Game getContext() {
		return context;
	}

	public void setContext(Game context) {
		this.context = context;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
