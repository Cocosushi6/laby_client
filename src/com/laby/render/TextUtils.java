package com.laby.render;

import java.awt.Font;

import org.newdawn.slick.TrueTypeFont;

public class TextUtils {
	
	private TrueTypeFont font;
	private Font awtFont;
	
	public TextUtils(int size) {
		awtFont = new Font("Times New Roman", Font.PLAIN, size);
		font = new TrueTypeFont(awtFont, true);
	}
	
	public void drawString(String text, int posX, int posY) {
		font.drawString(posX, posY, text);
	}
}
