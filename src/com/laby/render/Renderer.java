package com.laby.render;

import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glTranslatef;

import com.laby.main.Game;
import com.laby.world.Tile;

public class Renderer {
	
	private Game context;
	private TextUtils utils = new TextUtils(12);
	
	public Renderer(Game context) {
		this.context = context;
	}
	
	public void renderUI() {
		glPushMatrix();
		glTranslatef(context.getPlayer().getx(), context.getPlayer().gety(), 0);
		utils.drawString("PosX : " + context.getPlayer().getx() + ", PosY : " + context.getPlayer().gety(), 0, 0);
		utils.drawString("NewPosX : " + context.getPlayer().getnewX() + ", NewPosY : " + context.getPlayer().getnewY(), 0, -14);
		glPopMatrix();
	}
	
	public void render() {
		glPushMatrix();
		glTranslatef(-context.getCamera().getCamX(), -context.getCamera().getCamY(), 0);
		for(Tile t : context.getMap().getTiles()) {
			t.render();
		}
		context.getPlayer().render();
		for(int i : context.getIds()) {
			context.getEntities().get(i).render();
		}
		renderUI();
		glPopMatrix();
	}
}
