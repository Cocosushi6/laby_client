package com.laby.render;

import static org.lwjgl.opengl.GL11.*;

import com.laby.entities.Player;
import com.laby.entities.PlayerStates;
import com.laby.main.Game;
import com.laby.world.Map;
import com.laby.world.Tile;

public class Camera {
	
	private Map map;
	private Player focusEntity;
	private int camX = 0;
	private int camY = 0;
	private Game context;
	
	public Camera(Game context) {
		this.context = context;
		this.map = context.getMap();
		this.focusEntity = context.getPlayer();
	}
	
	public void moveCameraTo(int posX, int posY) {
		this.camX = posX;
		this.camY = posY;
	}
	
	public void update() {
		if(focusEntity.isMoving()) {
			if(focusEntity.getx() > Game.SCREEN_WIDTH / 2 && focusEntity.getx() < map.getWorldWidth() - Game.SCREEN_WIDTH / 2) {
				moveCameraTo(focusEntity.getx() - Game.SCREEN_WIDTH / 2, camY);
			}
			if(focusEntity.gety() > Game.SCREEN_HEIGHT / 2 && focusEntity.gety() < map.getWorldHeight() - Game.SCREEN_HEIGHT / 2) {
				moveCameraTo(camX, focusEntity.gety() - Game.SCREEN_HEIGHT / 2);
			}
		}
	}
	
	public void setFocusEntity(Player p) {
		this.focusEntity = p;
	}
	
	public int getCamX() {
		return this.camX;
	}
	
	public int getCamY() {
		return this.camY;
	}
	
}
