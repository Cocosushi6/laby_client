package com.laby.render;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.Timer;

import static org.lwjgl.opengl.GL11.*;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

import com.laby.util.Logger;

public class Animation implements ActionListener {
	private Texture texture;
	private Timer timer;
	private int cols = 0;
	private int rows = 0;
	private int currentCol = 0;
	private int currentRow = 0;
	private float width = 0;
	private float height = 0;
	private int spriteWidth = 0;
	private int spriteHeight = 0;
	private boolean stopAfterFirstAnimation = false;
	private boolean launched = false;
	
	public Animation(String spritesheetPath, int millisDelay, int cols, int rows) {
		this.timer = new Timer(millisDelay, this);
		this.cols = cols;
		this.rows = rows;
		//Width & height of a single sprite in spritesheet, in OpenGL texture rendering format (< 1)
		this.width = 1 / (float)this.cols; //For texture rendering, should be lower than 1 (OpenGL textures...)
		this.height = 1 / (float)this.rows; //Same here
		try {
			this.texture = TextureLoader.getTexture("PNG", new FileInputStream(spritesheetPath));
			this.spriteWidth = texture.getImageWidth() / cols; //64 for normal spritesheets
			this.spriteHeight = texture.getImageHeight() / rows; //Same here
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void launch() {
		timer.start();
		launched = true;
	}
	
	public void stop() {
		timer.stop();
		launched = false;
	}
	
	public void setCol(int newCol) {
		this.currentRow = newCol;
	}
	
	public boolean isAnimationLaunched() {
		return this.launched;
	}
	
	public void stopAfterFirstAnimation(boolean state) {
		this.stopAfterFirstAnimation = state;
	}
	
	public void render(int x, int y) {
		float x0 = (float)currentCol / cols;
		float x1 = width + (float)currentCol / cols;
		float y0 = (float)currentRow / rows;
		float y1 = height + (float)currentRow / rows;
		
		
		texture.bind();
		glPushMatrix();
		glTranslatef(x, y, 0);
		glBegin(GL_QUADS);
			glTexCoord2f(x0, y0);
			glVertex2f(0, 0);
			glTexCoord2f(x0, y1);
			glVertex2f(0, spriteHeight);
			glTexCoord2f(x1, y1);
			glVertex2f(spriteWidth, spriteHeight);
			glTexCoord2f(x1, y0);
			glVertex2f(spriteWidth, 0);
			
		glEnd();
		glPopMatrix();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(currentCol < cols - 1) {
			currentCol++;
		} else {
			if(stopAfterFirstAnimation) {
				stop();
			}
			currentCol = 0;
		}
	}
}
