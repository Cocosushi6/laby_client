package com.laby.main;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.Timer;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.util.glu.GLU;

import com.laby.entities.Entity;
import com.laby.entities.EntityArrow;
import com.laby.entities.Player;
import com.laby.entities.RemoteEntity;
import com.laby.events.GameEvent;
import com.laby.util.InputState;
import com.laby.util.Logger;
import com.laby.util.packets.GamePacketKey;
import com.laby.util.packets.PacketParser;
import com.laby.world.Tile;
import com.laby.world.Wall;


public class MainClient implements ActionListener {
	
	//UDP sockets 
	private DatagramSocket udpSocket;
	private DatagramPacket outgoing;
	private String lastUDPPacket;
	private boolean udpPacketReceived = false;
	
	private static long lastTick;
	
	//TCP sockets
	private Socket tcpSocket;
	private DataInputStream receive;
	private DataOutputStream send;
	private String lastTCPPacket;
	private boolean tcpPacketReceived = false;
	
	private int serverUDPPort, serverTCPPort;
	private InetAddress serverAddress;
	
	private PacketParser parser;
	
	private int localPort = 1568;
	private Timer timer;
	
	private Game game;
	
	public MainClient() {
		try {
			Display.setDisplayMode(new DisplayMode(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT));
			Display.setTitle("Networking test");
			Display.setResizable(false);
			Display.setFullscreen(false);
			Display.create();
		} catch(LWJGLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		//OpenGL init code
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 640, 480, 0, 1, -1);
		
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		Keyboard.enableRepeatEvents(true);
		
		//read config 
		readConfig();
		
		//Connection code
		connect();
		
		lastTick = System.currentTimeMillis();
		
		game = new Game(this);
		
		parser = new PacketParser(game);
		
		timer = new Timer(50, this);
		timer.start();
		
		Logger.log("Client initialisation done.");

		//Rendering code
		while(!Display.isCloseRequested()) {
		    //Security to avoid the last packet being parsed more than once
		    if(tcpPacketReceived) {
		        parsePacket(lastTCPPacket);
		        lastTCPPacket = "";
		        tcpPacketReceived = false;
		    }
		    if(udpPacketReceived) {
		    	parsePacket(lastUDPPacket);
		    	lastUDPPacket = "";
		    	udpPacketReceived = false;
		    }
		    
			if(game.isInitDone()) {
				glClear(GL_COLOR_BUFFER_BIT);
				
				
				int errcode;
				if((errcode = glGetError()) != GL_NO_ERROR) {
					Logger.log("GL error : " + GLU.gluErrorString(errcode) + "," + errcode);
				}
				
				game.input();
				game.update();
				game.render();
				
				Display.update();
				Display.sync(60);
			}
		}
		Display.destroy();
		System.exit(0);
	}
	
	public void registerBasicKeys() {
		parser.registerKey(new GamePacketKey("BEGIN", 
				new Vector<Object>(Arrays.asList(0L)),
				new GameEvent() {
					@Override
					public void perform(Game context, Vector<Object> values) {
						
					}
				}));
		parser.registerKey(new GamePacketKey("ENTITYDAMAGE", 
				new Vector<Object>(Arrays.asList(0, 0)), 
				new GameEvent() {
					@Override
					public void perform(Game context, Vector<Object> values) {
						Entity ent = context.getEntities().get((int)values.get(0));
	            		if(ent instanceof Player) {
	            			((Player)ent).giveDamage((int)values.get(1));
	            			Logger.log("given damage !");
	            		}
					}
				}));
		parser.registerKey(new GamePacketKey("WORLDCOMPLETE", 
				new Vector<Object>(), new GameEvent() {
					@Override
					public void perform(Game context, Vector<Object> values) {
						game.setInitDone(true);
						Logger.log("MAP INIT DONE");
					}
				}));
		parser.registerKey(new GamePacketKey("TILE", 
				new Vector<Object>(Arrays.asList(0, 0, "", false)), 
				new GameEvent() {
					@Override
					public void perform(Game context, Vector<Object> values) {
						if(!context.isInitDone()) {
							context.getMap().getTiles().add(new Tile((int)values.get(0), (int)values.get(1), 64, 64, (String)values.get(2), (boolean)values.get(3)));
							if((boolean)values.get(3)) {
								context.getMap().getSolids().add(new Tile((int)values.get(0), (int)values.get(1), 64, 64, (String)values.get(2), (boolean)values.get(3)));
							}
						}
					}
				}));
		parser.registerKey(new GamePacketKey("WALL", 
				new Vector<Object>(Arrays.asList(0, 0, "", false)), 
				new GameEvent() {
					@Override
					public void perform(Game context, Vector<Object> values) {
						if(!context.isInitDone()) {
							context.getMap().getTiles().add(new Wall((int)values.get(0), (int)values.get(1), 64, 64, (String)values.get(3), (String) values.get(2), true));
							context.getMap().getSolids().add(new Wall((int)values.get(0), (int)values.get(1), 64, 64, (String)values.get(3), (String) values.get(2), true));
						}
					}
				}));
	}
	
	public void connect() {
		try {
			//Create TCP socket for server on localhost 
			tcpSocket = new Socket(serverAddress, serverTCPPort);
			tcpSocket.setTcpNoDelay(true);
			
			//Initalise TCP streams for sending and receiving
			receive = new DataInputStream(tcpSocket.getInputStream());
			send = new DataOutputStream(tcpSocket.getOutputStream());
			
			//Initialise UDP socket (bidirectionnal)
			udpSocket = new DatagramSocket(localPort);
			byte[] buffer = new byte[65535];
			DatagramPacket incoming = new DatagramPacket(buffer, buffer.length);
			
			//Info message
			Logger.log("connected to server at address " + serverAddress.getHostAddress() + ", UDP port " + serverUDPPort + ", TCP port " + serverTCPPort);
			
			send.writeUTF(String.valueOf(localPort)); //Tell the server what UDP port to send to.
			
			new Thread(new Runnable() {
				@Override
				public void run() {
					while(true) {
						try {
							lastTCPPacket = receive.readUTF();//blocker
							
							//Print the packet received
							if(lastTCPPacket.contains("BEGIN")) {
								Logger.log(lastTCPPacket);
							}
							//Flag for the check loop
							tcpPacketReceived = true;
						} catch(IOException e) {
							e.printStackTrace();
							Logger.log("Disconnected By server");
							System.exit(1);
						}
					}
				}
			}).start();
			new Thread(new Runnable() {
				@Override
				public void run() {
					while(true) {
						try {
							udpSocket.receive(incoming); //blocker
							lastUDPPacket = new String(incoming.getData(), 0, incoming.getLength());
							udpPacketReceived = true;
						} catch(IOException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		} catch(IOException e) {
			e.printStackTrace();
			Logger.log("Failed to connect to server");
			System.exit(1);
		}
	}
	
	public void readConfig() {
		try {
			BufferedReader read = new BufferedReader(new FileReader("config.txt"));
			String line = "";
			while((line = read.readLine()) != null) {
				switch(line.split("=")[0]) {
					case "server_address" : 
						serverAddress = InetAddress.getByName(line.split("=")[1]);
						break;
					case "server_udp_port" : 
						serverUDPPort = Integer.parseInt(line.split("=")[1]);
						break;
					case "server_tcp_port" : 
						serverTCPPort = Integer.parseInt(line.split("=")[1]);
						break;
				}
			}
			read.close();
		} catch (IOException e) {
			Logger.log("Failed to read config file. Error : \n");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void parsePacket(String input) {
	    if(!input.equals(null)) {
	    	for(int i = 0; i < input.split(";").length; i++) {
	            String type = input.split(";")[i].split(":")[0];
	            String data = input.split(";")[i].split(":")[1];
	            switch(type) {
	            	case "ENTITYDAMAGE" : 
	            		Entity ent = game.getEntities().get(Integer.parseInt(data.split(",")[0]));
	            		if(ent instanceof Player) {
	            			((Player)ent).giveDamage(Integer.parseInt(data.split(",")[1]));
	            			Logger.log("given damage !");
	            		}
	            		break;
	                case "WORLDCOMPLETE" : 
	                    game.setInitDone(true);
	                    Logger.log("MAP INIT DONE");
	                    break;
	                case "TILE" :
	                	if(!game.isInitDone()) {
	                		game.getMap().getTiles().add(new Tile(
	                				Integer.parseInt(data.split(",")[0]), 
	                				Integer.parseInt(data.split(",")[1]),
	                				64, 64, data.split(",")[2], Boolean.parseBoolean(data.split(",")[3])));//Add a new tile with specific type
	                		if(Boolean.parseBoolean(data.split(",")[3])) {
	                			game.getMap().getSolids().add(new Tile(
	                					Integer.parseInt(data.split(",")[0]), 
	                					Integer.parseInt(data.split(",")[1]),
	                					64, 64, data.split(",")[2], 
	                					Boolean.parseBoolean(data.split(",")[3])));
	                		}
	                	}
	                	break;
	                case "WALL" :
	                	if(!game.isInitDone()) {
	                		game.getMap().getTiles().add(new Wall(Integer.parseInt(data.split(",")[0]), 
	                											Integer.parseInt(data.split(",")[1]), 
	                											64, 64, 
	                											data.split(",")[3], 
	                											data.split(",")[2], 
	                											true));
	                		game.getMap().getSolids().add(new Wall(Integer.parseInt(data.split(",")[0]), 
																Integer.parseInt(data.split(",")[1]), 
																64, 64, 
																data.split(",")[3], //subtype
																data.split(",")[2], //orientation
																true));
	                	}
	                	break;
	                case "WIDTH" :
	                	if(!game.isInitDone()) {
	                		game.getMap().setWorldWidth(Integer.parseInt(data));
	                		Game.setWORLD_WIDTH(Integer.parseInt(data));
	                	}
	                	Logger.log("Width : " + Integer.parseInt(data));
	                	break;
	                case "HEIGHT" :
	                	if(!game.isInitDone()) {
	                		game.getMap().setWorldHeight(Integer.parseInt(data));
	                		Game.setWORLD_HEIGHT(Integer.parseInt(data));
	                	}
	                	Logger.log("Height : " + Integer.parseInt(data));
	                	break;
	                case "CREATEENTITY" :
	                	switch(data.split(",")[0]) { //switch for entity type
	                		case "player" :
	                			game.addLocalEntity(Integer.parseInt(data.split(",")[1]), //ID
	                					new Player(Integer.parseInt(data.split(",")[2]),
	                							Integer.parseInt(data.split(",")[3]), game, true)); //Entity
	                			Logger.log("player joined game");
	                			break; // For now, only players can be added
	                		case "arrow" :
	                			game.addLocalEntity(Integer.parseInt(data.split(",")[1]), new EntityArrow(Integer.parseInt(data.split(",")[2]),
	                					Integer.parseInt(data.split(",")[3]), 
	                					Integer.parseInt(data.split(",")[4]),
	                					data.split(",")[5], game, true));
	                			break;
	                	}
	                	break;
	                case "ENTITY" :
	                	RemoteEntity entityConcerned = (RemoteEntity)game.getEntities().get(Integer.parseInt(data.split(",")[0]));
	                	if(entityConcerned != null) {
	                		if(Integer.parseInt(data.split(",")[0]) == game.getLocalID()) { //If the updated entity is this one, then this is a correction :
	                			Logger.log("correction received, data : " + type + ":" + data);

	                			//Correction code if online state is not the same (error of prediction) : 
	                			
	                			//Get the timestamp concerned
	                			long timestampUpdated = Long.parseLong(data.split(",")[1]);
	                			Logger.log("correction timestamp " + timestampUpdated);
	                			
	                			//Get local player state according to this timestamp
	                			Player oldState = game.getLocalPlayerStates().get(timestampUpdated);
	                			
	                			//Retrieve server informations 
	                			int serverX = Integer.parseInt(data.split(",")[2]);
	                			int serverY = Integer.parseInt(data.split(",")[3]);
	                			String serverDirection = data.split(",")[4];

	                			if(oldState != null) { //Just a check.
	                				if(!(oldState.getx() == serverX && oldState.gety() == serverY && oldState.getDirection().equals(serverDirection))) { //Check if old state is the same as server state at this time
	                					Logger.log("prediction error at timestamp " + timestampUpdated);
	                					Logger.log("oldState : " + oldState.getx() + "," + oldState.gety());
	                					Logger.log("serverState : " + serverX + "," + serverY);
	                					
	                					//Correct data
	                					game.getPlayer().setx(serverX);
	                					game.getPlayer().sety(serverY);
	                					game.getPlayer().setDirection(serverDirection);
	                				} else {
	                					Logger.log("prediction ok");
	                				}
	                			}             				
	                		} else {
	                			entityConcerned.setnewX(Integer.parseInt(data.split(",")[1]));
	                			entityConcerned.setnewY(Integer.parseInt(data.split(",")[2]));
	                			entityConcerned.setDirection(data.split(",")[3]);
//	                			entityConcerned.setMoving(Boolean.parseBoolean(data.split(",")[4]));
	                			//change if not working
	                		}
	                	}
	                	break;
	                case "PLAYER" :
	                	Player player = new Player(Integer.parseInt(data.split(",")[1]), Integer.parseInt(data.split(",")[2]), game, true);
	                    game.setPlayer(player);
	                    game.setLocalID(Integer.parseInt(data.split(",")[0]));
	                    Display.setTitle("Networking test " + game.getLocalID());
	                    game.addLocalEntity(Integer.parseInt(data.split(",")[0]), player); //ID
	                    Logger.log("Player created, position : " + player.getx() + "," + player.gety());
	                    break;
	                case "REMOVE" : 
	                	game.getEntities().remove(Integer.parseInt(data.split(",")[0]));
	                	for(int d = 0; d < game.getIds().size(); d++) {
	                		if(game.getIds().get(d) == Integer.parseInt(data.split(",")[0])) {
	                			game.getIds().remove(d);
	                		}
	                	}
	                	break;
	            }
	        }
	    }
	}

	public void sendUDPPacket(String data) {
		if(game.isInitDone()) {
			try {
				outgoing = new DatagramPacket(data.getBytes(), data.getBytes().length, InetAddress.getByName("127.0.0.1"), 7777);
				udpSocket.send(outgoing);
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void sendTCPPacket(String data) {
		try {
			DataOutputStream send = new DataOutputStream(tcpSocket.getOutputStream());
			send.writeUTF(data);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
    @Override
    public void actionPerformed(ActionEvent arg0) {
    	lastTick = System.currentTimeMillis();
    	if(game.isInitDone() && !game.getUnprocessedInputStates().isEmpty()) {
    		
    		Logger.log("Unprocessed inputs since last tick : " + game.getUnprocessedInputStates().size());
    		
    		game.saveCurrentState();
    		Logger.log("Current player state saved !");
            String packetUpdate = "BEGIN:" + game.getLocalID() + "," + lastTick + ";";
            
            for(Enumeration<Long> e = game.getUnprocessedInputStates().keys(); e.hasMoreElements();) {
            	long timePressed = e.nextElement();
            	InputState stateAtTime = game.getUnprocessedInputStates().get(timePressed);
            	
            	if(stateAtTime != null) {
            		packetUpdate += "KEY:" + timePressed + "," + stateAtTime.getKey() + "," + stateAtTime.getKeyState() + ";";
            	}
            }
            
            game.clearOldStates();
            Logger.log("Old states cleaned");
            
            try {
            	outgoing = new DatagramPacket(packetUpdate.getBytes(), packetUpdate.getBytes().length, serverAddress, serverUDPPort);
                udpSocket.send(outgoing);
                Logger.log("Sent ! packet string : + \"" + packetUpdate + "\"");
            } catch(IOException e) {
            	e.printStackTrace();
            }
        }
    }
    
    public static long getLastTick() {
    	return lastTick;
    }
    
	public static void main(String[] args) {
		new MainClient();
	}
}