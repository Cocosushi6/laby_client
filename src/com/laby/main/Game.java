package com.laby.main;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import org.lwjgl.input.Keyboard;

import com.laby.entities.Entity;
import com.laby.entities.Player;
import com.laby.entities.PlayerStates;
import com.laby.render.Camera;
import com.laby.render.Renderer;
import com.laby.util.InputState;
import com.laby.world.Map;

public class Game {
	
	private static int WORLD_WIDTH = 0;
	private static int WORLD_HEIGHT = 0;
	public static final int SCREEN_WIDTH = 640;
	public static final int SCREEN_HEIGHT = 480;
	
	private boolean initDone = false;
	private Map map = new Map();
	private Camera camera;
	private Renderer renderer;
	private Player player;
	private ConcurrentHashMap<Long, Player> localPlayerStates = new ConcurrentHashMap<Long, Player>();
	
	private ConcurrentHashMap<Long, InputState> inputStates = new ConcurrentHashMap<Long, InputState>();
	private ConcurrentHashMap<Long, InputState> unprocessedInputStates = new ConcurrentHashMap<Long, InputState>();
	
	private ConcurrentHashMap<Integer, Entity> entities = new ConcurrentHashMap<Integer, Entity>(); //Players (called "Entities")
	private Vector<Integer> ids = new Vector<Integer>();
	private int localID = 0;
	
	private MainClient client;
	
	public Game(MainClient gameClient) {
		this.map = new Map();
		this.player = new Player(this, false);
		this.camera = new Camera(this);
		this.renderer = new Renderer(this);
		this.client = gameClient;
	}
	
	public Game(Map map, Player player, MainClient gameClient) {
		this.map = map;
		this.player = player;
		this.camera = new Camera(this);
		this.renderer = new Renderer(this);
		this.client = gameClient;
	}
	
	public void update() {
		for(int i : ids) {
			entities.get(i).update(MainClient.getLastTick());
		}
		camera.update();
	}
	
	public void render() {
		if(renderer != null) {
			renderer.render();
		}
	}
	
	//Move input method to View (MainClient)
	public void input() {
		while(Keyboard.next()) {
			long timestamp = MainClient.getLastTick();
			if(Keyboard.getEventKey() == Keyboard.KEY_Z) {
				if(Keyboard.getEventKeyState()) {
					if(!Keyboard.isRepeatEvent()) {
						player.setDirection("up");
						player.setState(PlayerStates.WALKING);
					}
					player.move();
					keyHasBeenPressed(timestamp, "up", true);
				} else {
					player.setState(PlayerStates.STANDING);
				}
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_S) {
				if(Keyboard.getEventKeyState()) {
					if(!Keyboard.isRepeatEvent()) {
						player.setDirection("down");
						player.setState(PlayerStates.WALKING);
					}
					player.move();
					keyHasBeenPressed(timestamp, "down", true);
				} else {
					player.setState(PlayerStates.STANDING);
				}
				
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_D) {
				if(Keyboard.getEventKeyState()) {
					if(!Keyboard.isRepeatEvent()) {
						player.setDirection("right");
						player.setState(PlayerStates.WALKING);
					}
					player.move();
					keyHasBeenPressed(timestamp, "right", true);
				} else {
					player.setState(PlayerStates.STANDING);
				}
				
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_Q) {
				if(Keyboard.getEventKeyState()) {
					if(!Keyboard.isRepeatEvent()) {
						player.setDirection("left");
						player.setState(PlayerStates.WALKING);
					}
					player.move();
					keyHasBeenPressed(timestamp, "left", true);
				} else {
					player.setState(PlayerStates.STANDING);
				}
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_B && Keyboard.getEventKeyState()) {
				player.setCurrentWeapon("bow");
				keyHasBeenPressed(timestamp, "bow", true);
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_SPACE && Keyboard.getEventKeyState()) {
				player.useWeapon();
				keyHasBeenPressed(timestamp, "fire", true);
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_UP && Keyboard.getEventKeyState()) {
				camera.moveCameraTo(camera.getCamX(), camera.getCamY() - 10);
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_DOWN && Keyboard.getEventKeyState()) {
				camera.moveCameraTo(camera.getCamX(), camera.getCamY() + 10);
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_LEFT && Keyboard.getEventKeyState()) {
				camera.moveCameraTo(camera.getCamX() - 10, camera.getCamY());
			}
			if(Keyboard.getEventKey() == Keyboard.KEY_RIGHT && Keyboard.getEventKeyState()) {
				camera.moveCameraTo(camera.getCamX() + 10, camera.getCamY());
			}
		}
	}
	
	public void saveCurrentState() {
		localPlayerStates.put(MainClient.getLastTick(), player);
	}
	
	public void keyHasBeenPressed(long time, String keyCode, boolean state) {
		inputStates.put(time, new InputState(keyCode, state, time));
		unprocessedInputStates.put(time, new InputState(keyCode, state, time));
	}
	
	public void clearOldStates() {
		unprocessedInputStates.clear();
		for(Enumeration<Long> e = inputStates.keys(); e.hasMoreElements();) {
			long curKey = e.nextElement();
			if(System.currentTimeMillis() - 5000 > curKey) { //Check if inputs are older than 5 seconds before now, and clear them
				inputStates.remove(curKey);
			}
		}
	}
	
	public ConcurrentHashMap<Long, Player> getLocalPlayerStates() {
		return this.localPlayerStates;
	}

	public static int getWORLD_WIDTH() {
		return WORLD_WIDTH;
	}

	public static void setWORLD_WIDTH(int wORLD_WIDTH) {
		WORLD_WIDTH = wORLD_WIDTH;
	}

	public static int getWORLD_HEIGHT() {
		return WORLD_HEIGHT;
	}

	public static void setWORLD_HEIGHT(int wORLD_HEIGHT) {
		WORLD_HEIGHT = wORLD_HEIGHT;
	}

	public void addLocalEntity(Integer id, Entity ent) {
		this.entities.put(id, ent);
		this.ids.add(id);
	}
	
	public void createRemoteEntity(Entity ent, String type) {
		String data = "BEGIN:" + this.localID + "," + System.currentTimeMillis() + ";";
		data += "CREATEENTITY:" + type + "," + ent.getx() + "," + ent.gety() + "," + ent.getSpeed() + "," + ent.getDirection() + "," + this.localID + ";";
		data += "END:0;";
		client.sendTCPPacket(data);
	}
	
	public ConcurrentHashMap<Long, InputState> getInputStates() {
		return this.inputStates;
	}
	
	public ConcurrentHashMap<Long, InputState> getUnprocessedInputStates() {
		return this.unprocessedInputStates;
	}

	public boolean isInitDone() {
		return initDone;
	}

	public void setInitDone(boolean initDone) {
		this.initDone = initDone;
	}

	public int getLocalID() {
		return localID;
	}

	public void setLocalID(int localID) {
		this.localID = localID;
	}

	public Map getMap() {
		return map;
	}

	public Camera getCamera() {
		return camera;
	}

	public Player getPlayer() {
		return player;
	}

	public ConcurrentHashMap<Integer, Entity> getEntities() {
		return entities;
	}

	public Vector<Integer> getIds() {
		return ids;
	}
	
	public void setPlayer(Player player) {
		if(!initDone) {
			this.player = player;
			this.camera = new Camera(this);
		} else {
			this.player = player;
			this.camera.setFocusEntity(this.player);
			entities.replace(localID, player);
		}
	}
}
