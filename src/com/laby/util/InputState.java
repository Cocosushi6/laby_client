package com.laby.util;

public class InputState {
	
	private boolean keyState;
	private String key;
	private long timestampPressed;
	
	public InputState(String key, boolean keyState, long timestamp) {
		this.key = key;
		this.keyState = keyState;
		this.timestampPressed = timestamp;
	}

	public boolean getKeyState() {
		return keyState;
	}

	public void setKeyState(boolean keyState) {
		this.keyState = keyState;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public long getTimestampPressed() {
		return timestampPressed;
	}

	public void setTimestampPressed(long timestampPressed) {
		this.timestampPressed = timestampPressed;
	}
}