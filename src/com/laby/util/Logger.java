package com.laby.util;

import java.util.Date;

public class Logger {

	public static void log(String message) {
		Date date = new Date();
		System.out.println("[" + date + "] " + message);
	}
	
}
