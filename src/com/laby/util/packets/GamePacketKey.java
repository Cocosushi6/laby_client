package com.laby.util.packets;

import java.awt.Event;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import com.laby.events.GameEvent;
import com.laby.main.Game;


//For use with the PacketParser class.
public class GamePacketKey {
	
	private Vector<GameEvent> listeners = new Vector<GameEvent>();
	private Vector<Object> values;
	private String keyName = "";
	
	public GamePacketKey(String keyName, Vector<Object> values, GameEvent callback) {
		this.values = values;
		this.keyName = keyName;
		this.listeners.add(callback);
	}
	
	public void process(Game context) {
		for(GameEvent event : listeners) {
			event.perform(context, values);
		}
	}
	
	public String getKeyName() {
		return this.keyName;
	}
	
	public void setKeyName(String name) {
		this.keyName = name;
	}
	
	public void addValue(Object value) {
		values.add(value);
	}

	public Vector<Object> getValues() {
		return values;
	}
}
