package com.laby.util.packets;

import java.util.Arrays;
import java.util.Vector;

import org.lwjgl.opengl.Display;

import com.laby.entities.Entity;
import com.laby.entities.EntityArrow;
import com.laby.entities.Player;
import com.laby.entities.RemoteEntity;
import com.laby.events.GameEvent;
import com.laby.main.Game;
import com.laby.world.Tile;
import com.laby.world.Wall;

public class PacketParser {
	
	private Vector<GamePacketKey> keys = new Vector<GamePacketKey>();
	private Game game;
	
	public PacketParser(Game game) {
		this.game = game;
	}
	
	public void registerKey(GamePacketKey key) {
		keys.add(key);
	}
	
	public synchronized void parse(String input) {
		if(!input.equals(null)) {
	    	for(int i = 0; i < input.split(";").length; i++) {
	            String type = input.split(";")[i].split(":")[0];
	            String data = input.split(";")[i].split(":")[1];
	            for(GamePacketKey key : keys) {
	            	if(type.equals(key.getKeyName())) {
	            		int index = 0;
	            		for(Object obj : key.getValues()) {
	            			if(obj instanceof Integer) {
	            				int value = Integer.parseInt(data.split(",")[index]);
	            				key.getValues().set(index, value);
	            			} else if(obj instanceof String) {
	            				String value = data.split(",")[index];
	            				key.getValues().set(index, value);
	            			} else if(obj instanceof Float) {
	            				float value = Float.parseFloat(data.split(",")[index]);
	            				key.getValues().set(index, value);
	            			}
	            			index++;
	            		}
	            		key.process(game);
	            	}
	            }
	    	}
	    }
	}
	
}
