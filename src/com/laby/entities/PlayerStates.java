package com.laby.entities;

public enum PlayerStates {
	WALKING(true), SHOOTING(false), STRIKING(true), STANDING(false);
	
	private boolean moving = false;
	
	private PlayerStates(boolean moving) {
		this.moving = moving;
	}
	
	public boolean isMovingState() {
		return moving;
	}
}
