package com.laby.entities;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.newdawn.slick.opengl.Texture;

import com.laby.main.Game;

public class Entity  {
	
	protected int x, y;
	protected String direction = "down";
	protected List<String> possibleDirections = Arrays.asList("up", "down", "right", "left");
	protected int id;
	protected HashMap<String, Texture> textures = new HashMap<String, Texture>();
	protected int width = 45, height = 60;
	protected int spriteWidth = 64, spriteHeight = 64;
	protected float textureStart = 0.50f;
	protected float textureEnd = 0.75f;
	protected boolean moving = false;
	protected Game context;
	protected int speed = 5;
	
	public Entity(Game context) {
		this.context = context;
	}
	
	public Entity(int x, int y, Game context) {
		this.x = x;
		this.y = y;
		this.context = context;
	}
	
	public void update(long tick) {
	        switch(direction) {
	            case "up" :
	                textureStart = 0f;
	                textureEnd = 0.25f;
	                break;
	            case "left" :
	                textureStart =  0.25f;
	                textureEnd =  0.50f;
	                break;
	            case "down" :
	                textureStart = 0.50f;
	                textureEnd = 0.75f;
	                break;
	            case "right" :
	                textureStart = 0.75f;
	                textureEnd = 1f;
	                break;
	        }
	}
	
	public boolean collision() {
		return false;
	}
	
	public void render() {
		glColor3f(1, 1, 1);
		glPushMatrix();     
        glTranslatef(this.x, this.y, 0);
        glBegin(GL_QUADS);
            glVertex2f(0, 0);
            glVertex2f(0, spriteHeight);
            glVertex2f(spriteWidth, spriteHeight);
            glVertex2f(spriteWidth, 0);
        glEnd();
        glPopMatrix();
	}
	
	public int getx() {
		return this.x;
	}
	
	public int gety() {
		return this.y;
	}
	
	public void setx(int x) {
		if(x >= 0 && x < Game.getWORLD_WIDTH()) {
			this.x = x;
		}
	}
	
	public void sety(int y) {
		if(y >= 0 && y < Game.getWORLD_HEIGHT()) {
			this.y = y;
		}
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public int getID() {
		return this.id;
	}
	
	public String getDirection() {
	    return this.direction;
	}

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setDirection(String direction) {
    	if(!possibleDirections.contains(direction)) {
    		direction = "down";
    	}
    	this.direction = direction;
    }
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public int getSprite_width() {
		return spriteWidth;
	}

	public void setSprite_width(int spriteWidth) {
		this.spriteWidth = spriteWidth;
	}

	public int getSprite_height() {
		return spriteHeight;
	}

	public void setSprite_height(int spriteHeight) {
		this.spriteHeight = spriteHeight;
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
}
