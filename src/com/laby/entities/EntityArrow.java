package com.laby.entities;

import java.io.FileInputStream;
import java.io.IOException;

import org.newdawn.slick.opengl.TextureLoader;

import com.laby.main.Game;

import static org.lwjgl.opengl.GL11.*;

public class EntityArrow extends RemoteEntity {
	
	protected float rotateAngle = 0f;
	
	public EntityArrow(Game context, boolean isRemote) {
		super(context, isRemote);
	}
	
	public EntityArrow(int x, int y, int speed, String direction, Game context, boolean isRemote) {
		super(x, y, context, isRemote);
		this.direction = direction;
		this.speed = speed;
		switch(direction) {
			case "right" : 
				this.rotateAngle = 0f;
				break;
			case "left" : 
				this.rotateAngle = 180f;
				break;
			case "down" : 
				this.rotateAngle = 90f;
				break;
			case "up" :
				this.rotateAngle = 270f;
				break;
		}
		try {
			this.textures.put("default", TextureLoader.getTexture("PNG", new FileInputStream("res/arrow.png" )));
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void render() {
		textures.get("default").bind();
		glPushMatrix();
			glTranslatef(this.x, this.y, 0);
			glRotatef(rotateAngle, 0, 0, 1);
				glBegin(GL_QUADS);
					glTexCoord2f(0, 0);
					glVertex2f(0, 0);
					glTexCoord2f(0, 1);
					glVertex2f(0, 8);
					glTexCoord2f(1, 1);
					glVertex2f(32, 8);
					glTexCoord2f(1, 0);
					glVertex2f(32, 0);
				glEnd();
		glPopMatrix();
	}
}
