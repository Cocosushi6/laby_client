package com.laby.entities;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.newdawn.slick.opengl.TextureLoader;

import com.laby.gameplay.Character;
import com.laby.main.Game;
import com.laby.render.Animation;
import com.laby.util.Logger;
import com.laby.weapons.Bow;
import com.laby.weapons.Weapon;
import com.laby.world.Tile;

public class Player extends RemoteEntity {
	
	private PlayerStates state = PlayerStates.STANDING;
	private ConcurrentHashMap<String, Animation> animations = new ConcurrentHashMap<String, Animation>();
	private Animation currentAnimation;
	private HashMap<String, Weapon> weapons = new HashMap<String, Weapon>();
	private Weapon currentWeapon = null;
	private Character character = new Character(100, 100);
	private boolean stateChanged = false;
	private boolean weaponInUse = false;
	private long timeSinceShoot = 0L;
	
	public Player(Game context, boolean isRemote) {
		super(context, isRemote);
		initTexture();
	}
	
	public Player(int x, int y, Game context, boolean isRemote) {
		super(x, y, context, isRemote);
		Logger.log("new player initialised");
		this.context = context;
		initTexture();
		initWeapons();
	}
	
	public void giveDamage(int damage) {
		this.character.giveDamage(damage);
	}
	
	public void useWeapon() {
		if(this.currentWeapon != null && !weaponInUse) {
			weaponInUse = true;
			currentWeapon = weapons.get("bow");
			if(currentWeapon instanceof Bow) {
				setState(PlayerStates.SHOOTING);
				timeSinceShoot = System.currentTimeMillis();
			} 
		}
	}
	
	protected void initWeapons() {
		Weapon bow = new Bow(context, this, "bow");
		weapons.put("bow", bow);
	}
	
	protected void initTexture() {
		Logger.log("player texture initialised");
		try {
			textures.put("default", TextureLoader.getTexture("PNG", new FileInputStream(new File("res/warrior_static.png"))));
		} catch(IOException e) {
			e.printStackTrace();
		}
		Animation movingAnimation = new Animation("res/warrior_moving.png", 100, 8, 4);
		Animation shootingAnimation = new Animation("res/warrior_shooting.png", 100, 8, 4);
		shootingAnimation.stopAfterFirstAnimation(true);
		animations.put("walking", movingAnimation);
		animations.put("shooting", shootingAnimation);
		currentAnimation = null;
	}
	
	@Override
	public boolean collision() {
		for(Tile t : context.getMap().getSolids()) {
			if(this.getBounds().intersects(t.getBounds())) {
				return true;
			}
		}
		for(Entity ent : context.getEntities().values()) {
			if(ent.getID() != this.getID()) {
				if(this.getBounds().intersects(ent.getBounds())) {
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void update(long tick) {
		super.update(tick);
		if(stateChanged) {
			//Stop current animation before switching
			if(currentAnimation != null) {
				currentAnimation.stop();
			}
			switch(state) {
				case WALKING : 
					currentAnimation = animations.get("walking");
					currentAnimation.launch();
					break;
				case SHOOTING:
					currentAnimation = animations.get("shooting");
					currentAnimation.setCol(getDirectionNumber());
					currentAnimation.launch();
					break;
				case STANDING:
					currentAnimation = null;
					break;
				case STRIKING:
					break;
			}
			stateChanged = false;
		}
		//Launch arrow at right frame in animation 
		if(state == PlayerStates.SHOOTING) {
			if(System.currentTimeMillis() - timeSinceShoot >= 600 && weaponInUse) {
				currentWeapon.use(this.direction);
				weaponInUse = false;
			}
			if(System.currentTimeMillis() - timeSinceShoot >= 800) {
				setState(PlayerStates.STANDING);
			}
		}
	}

	@Override
	public void render() {
		if(state != PlayerStates.STANDING) {
			currentAnimation.render(this.x, this.y);
		} else {
			textures.get("default").bind();
			glPushMatrix();		
			glTranslatef(this.x, this.y, 0);
			glBegin(GL_QUADS);
				glTexCoord2f(0, textureStart);
				glVertex2f(0, 0);
				glTexCoord2f(0, textureStart + 0.25f);
				glVertex2f(0, spriteHeight);
				glTexCoord2f(1, textureStart + 0.25f);
				glVertex2f(spriteWidth, spriteHeight);
				glTexCoord2f(1, textureStart);
				glVertex2f(spriteWidth, 0);
			glEnd();
			glPopMatrix();
		}
	}
	
	public void move() {
		if(!collision()) {
			switch(direction) {
				case "up" :
					this.newY = this.y - speed * 4;
					break;
				case "down" :
					this.newY = this.y + speed * 4;
					break;
				case "left" : 
					this.newX = this.x - speed * 4;
					break;
				case "right" :
					this.newX = this.x + speed * 4;
					break;
			}
		}
	}
	
	@Override
	public Rectangle getBounds() {
		if(state.isMovingState()) {
			switch(direction) {
				case "up" :
					return new Rectangle(this.x + (spriteWidth / 2 - width / 2), this.y - speed * 4 + (spriteHeight / 2 - height / 2), this.width, this.height);
				case "down" : 
					return new Rectangle(this.x + (spriteWidth / 2 - width / 2), this.y + speed * 4+ (spriteHeight / 2 - height / 2), this.width, this.height);
				case "left" : 
					return new Rectangle(this.x - speed * 4 + (spriteWidth / 2 - width / 2), this.y + (spriteHeight / 2 - height / 2), this.width, this.height);
				case "right" : 
					return new Rectangle(this.x + speed * 4 + (spriteWidth / 2 - width / 2), this.y + (spriteHeight / 2 - height / 2), this.width, this.height);
			} //Speed is *4 here because it is too in move(), where the collision is checked.
		}
		return new Rectangle(this.x, this.y, this.width, this.height);
	}
	
	//Simple util method
	public int getDirectionNumber() {
		switch(direction) {
			case "left" :
				return 1;
			case "right" : 
				return 3;
			case "up" : 
				return 0;
			case "down" : 
				return 2;
		}
		return 0;
	}
	
	public void setState(PlayerStates state) {
		this.stateChanged = true;
		Logger.log(System.currentTimeMillis() + ", new state : " + state);
		if(state.isMovingState()) {
			this.moving = true;
		} else {
			this.moving = false;
		}
		this.state = state;
	}
	
	public void setCurrentWeapon(String weapon) {
		this.currentWeapon = weapons.get(weapon);
	}
	
	public Weapon getCurrentWeapon() {
		return this.currentWeapon;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Player)) {
			return false;
		}
		Player p = (Player)obj;
		return p.getx() == this.newX && p.gety() == this.newY && p.getDirection().equals(this.direction);
	}
	
	@Override
	public void setDirection(String direction) {
		if(possibleDirections.contains(direction)) {
			if(currentAnimation != null) {
				switch(direction) {
					case "left" : 
						currentAnimation.setCol(1);
						break;
					case "right" :
						currentAnimation.setCol(3);
						break;
					case "up" : 
						currentAnimation.setCol(0);
						break;
					case "down" :
						currentAnimation.setCol(2);
						break;
				}
			} else if(state == PlayerStates.STANDING) {
				switch(direction) {
					case "left" : 
						textureStart = 0.25f;
						break;
					case "right" : 
						textureStart = 0.75f;
						break;
					case "up" : 
						textureStart = 0.0f;
						break;
					case "down" : 
						textureStart = 0.5f;
						break;
				}
			}
		} else {
			direction = "down";
		}
		this.direction = direction;
	}
	
	public PlayerStates getState() {
		return this.state;
	}
	
	public Character getCharacter() {
		return this.character;
	}
} 