package com.laby.entities;

import com.laby.main.Game;

public class RemoteEntity extends Entity {

	protected int newY, newX;
	protected boolean isRemote = false;
	
	public RemoteEntity(Game context, boolean remote) {
		super(context);
		this.isRemote = remote;
	}
	
	public RemoteEntity(int x, int y, Game context, boolean remote) {
		super(x, y, context);
		this.isRemote = remote;
	}
	
	@Override
	public void update(long tick) {
			switch(direction) {
				case "up" :
					textureStart = 0f;
					textureEnd = 0.25f;
					if(this.newY < this.y) {
						if(this.y - this.newY >= speed) {
							this.y -= speed;
						} else {
							this.y -= this.y - this.newY;
						}
					}
					break;
				case "left" :
					textureStart =  0.25f;
					textureEnd =  0.50f;
					if(this.newX < this.x) {
						if(this.x - this.newX >= speed) {
							this.x -= speed;
						} else {
							this.x -= this.x - this.newX;
						}
					}
					break;
				case "down" :
					textureStart = 0.50f;
					textureEnd = 0.75f;
					if(this.newY > this.y) {
						if(this.newY - this.y >= speed) {
							this.y += speed;
						} else {
							this.y += this.newY - this.y;
						}
					}
					break;
				case "right" :
					textureStart = 0.75f;
					textureEnd = 1f;
					if(this.newX > this.x) {
						if(this.newX - this.x >= speed) {
							this.x += speed;
						} else {
							this.x += this.newX - this.x;
						}
					}
					break;
			}
	}

	@Override
	public void setDirection(String direction) {
		switch(direction) {
			case "down" :
				if(newX != x) {
					if(newX > x) {
						if(this.newX - this.x >= 10) {
							this.x += 10;
						} else {
							this.x += this.newX - this.x;
						}
					} else {
						if(this.x - this.newX >= 10) {
							this.x -= 10;
						} else {
							this.x -= this.x - this.newX;
						}
					}
				} else {
					this.direction = direction;
				}
				break;
			case "right" :
				if(newY != y) {
					if(newY > y) {
						if(this.newY - this.y >= 10) {
							this.y += 10;
						} else {
							this.y += this.newY - this.y;
						}
					} else {
						if(this.y - this.newY >= 10) {
							this.y -= 10;
						} else {
							this.y -= this.y - this.newY;
						}
					}
				} else {
					this.direction = direction;
				}
				break;
			case "left" :
				if(newY != y) {
					if(newY > y) {
						if(this.newY - this.y >= 10) {
							this.y += 10;
						} else {
							this.y += this.newY - this.y;
						}
					} else {
						if(this.y - this.newY >= 10) {
							this.y -= 10;
						} else {
							this.y -= this.y - this.newY;
						}
					}
				} else {
					this.direction = direction;
				}
				break;
			case "up" :
				if(newX != x) {
					if(newX > x) {
						if(this.newX - this.x >= 10) {
							this.x += 10;
						} else {
							this.x += this.newX - this.x;
						}
					} else {
						if(this.x - this.newX >= 10) {
							this.x -= 10;
						} else {
							this.x -= this.x - this.newX;
						}
					}
				} else {
					this.direction = direction;
				}
				break;
		}
	}

	public int getnewX() {
		return newX;
	}

	public void setnewX(int newX) {
		this.newX = newX;
	}

	public int getnewY() {
		return newY;
	}

	public void setnewY(int newY) {
		this.newY = newY;
	}


}
