package com.laby.events;

import java.util.Vector;

import com.laby.main.Game;

public interface GameEvent {
	
	public void perform(Game context, Vector<Object> values);
	
}
